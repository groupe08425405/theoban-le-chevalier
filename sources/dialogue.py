# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 14:35:40 2024

@author: robca
"""
import cgi
# Notre script va retourner du HTML
print("Content-type: text/html; charset=UTF-8\n")
print("""
      <!DOCTYPE html>
      <html>
          <head>
              <meta charset="UTF-8">
              <title> Ma page </title>
              <link href="styles/style1.css" />
              <script> 
                  var a=1;
                  function switchtext(){
                      a=a+1;
                      if (a==2){
                        document.getElementById("text").innerHTML="texte2";
                        };
                      if (a==3){
                        document.getElementById("text").innerHTML="texte3";
                        };
                      }
              </script>
           </head>
           <body>
               <p id="text">texte1</p>
               <input type="button" onclick=switchtext()>PASSER</input>
           </body>
       </html>""")
