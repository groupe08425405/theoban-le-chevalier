# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 16:26:43 2024

@author: rcarbonnet
"""

import cgi
import cgitb
import csv
cgitb.enable()
form=cgi.FieldStorage()
html="""
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link href="styles/style_combats.css" rel="stylesheet"/>
</head>
<body>
"""
print(html)
try:
    pvjoueur=int(form.getvalue("pvjoueur"))
    pvboss=int(form.getvalue("pvboss"))
except:
    pvjoueur=100
    pvboss=200
affpvjoueur=int(pvjoueur*150/100)
affpvboss=int(pvboss*150/200)

print(f"""<div id="affichepvboss">
              <h1> PV BOSS : {pvboss} pv  </h1>
              <div class="pv"><div style="height:10px; width: {affpvboss}px; background-color: #7FFF00;"></div>
          </div></div>
          <div id="affichepvjoueur">
              <h1> PV JOUEUR : {pvjoueur} pv </h1>
              <div class="pv"><div style="height:10px; width: {affpvjoueur}px; background-color: #7FFF00;"></div>
          </div></div>""")

print("""<div style="width: 40%; float: left;"><img id="chevalier" src="images/chevalier.png"></img>"</div><div style="width: 40%; float: right;"><img id="monstre" src="images/monstre.png"></img></div>""")
print(f"""
      <div style="border: solid black 3px;
	background-color: #C9C9C9;
	position: fixed;
    bottom: 0;
	height: 30%;
	width: 95%;">
          <form method="post" action="pages_combats2.py">
          <h2>Que faites-vous ?</h2>
          <div id="boutongauche"><input type="radio" name="choix" id="bouton1" value="combattre"  checked><label  for="bouton1"> COMBATTRE</label></div>   
          <div id="boutondroite"><input type="radio" name="choix"  id="bouton2"  value="fuire" ><label for="bouton2"> FUIRE</label></div>
          </br></br>
          <input id="bouton_valider"type="submit" value="VALIDER">
          <input type="radio" name="pvjoueur" value="{pvjoueur}" class="cache" checked>
          <input type="radio" name="pvboss" value="{pvboss}" class="cache" checked>
      </div>
      """)



print("""</form></body></html>""")