# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 08:57:50 2024

@author: rcarbonnet
"""

import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb


# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_page2.css" rel="stylesheet" />
    </head>
    <body>
    """)
try:
    if form.getvalue("armure"):
        armure=form.getvalue("armure")
        if armure=="or":
            print("""<p>Tu as choisi l'armure en or.</p>
            <h1>Chapitre 2 : </h1>
            <p>Juste après avoir pris possession de  ton armure...</p>
            <p>La reine t'annonce que tu doit sauver leur fille enlever dans une grotte, à deux jours de marche, environ, d'ici.</p> 
            <img src="images/princesse2.png" >
            
            <form action="chemin.py">
                <button type="submit">Tu n'hésite pas une seconde et pars, la princesse est en danger de mort !</button>
            </form><form method="post" action="perdu1.py">
                <button type="submit">Après avoir hésité un moment, tu décide de ne pas aller sauver la princesse, car c'est trop risqué.</button>
            </form>""")
        else:
            print("""<h1>Tu as choisi l'armure en argent</h1>
                  <h1>Chapitre 2 : </h1>
                  <p></p>
                  <p></p>
                  <p></p>
                  <form action="dialogue.py">
                  <button type="submit"><img id="" src=""></button>
                  <button type="submit"><img id="" src=""></button>
                  </form>""")
    else:
        raise Exception
except:
    print("<p>Le choix de votre armure n'a pas été transmis. Veuillez rééssayer.</p>")

print("""</body>
</html>
""")

