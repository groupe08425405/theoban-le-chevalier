# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 14:05:42 2024

@author: rcarbonnet
"""

import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb


# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_auberge.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""<h1>Chapitre A : L'auberge</h1>
<p>La nuit comence à tomber et tu arrives dans une auberge.</p>
<p>Le propriétaire te loue une chambre pour la nuit, et maintenant tu vas dans la pièce commune. Quand tu arrives, il y a deux personnes.</p>

<form action="perdu_auberge.py">
    <button type="submit">Tu choisis de parler avec la personne qui a l'air super sympatique et qui te propose d'aller faire une promenade dehors.</button>
</form>
<form method="post" action="auberge2.py">
    <button type="submit">Tu choisis de parler avec la personne qui a l'air gentille mais qui a un petit peu bu.</button>
</form>""")

print("""</body>
</html>
""")
