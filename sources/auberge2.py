# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 15:13:32 2024

@author: rcarbonnet
"""

import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb


# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_auberge.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""<h1>Chapitre A : L'auberge</h1>
<p>La personne t'indique la direction de la grotte où a été emmené la princesse.</p>
<p>Le lendemain, tu pars en direction de la grotte.</p>
<form action="go_combat.py">
    <button type="submit">Allez en direction de la grotte</button>
</form>""")

print("""</body>
</html>
""")