# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 15:06:01 2024

@author: rcarbonnet
"""


import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb


# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_auberge.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""<h1>Combat final</h1>
<p>Tu arrives devant la grotte et tu t'apprêtes à combattre le monstre.</p>
<form action="pages_combats1.py">
    <button type="submit">Allez au combat</button>
</form>""")

print("""</body>
</html>
""")