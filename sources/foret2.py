# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 14:46:54 2024

@author: rcarbonnet
"""

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_chemin.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""<h1>La forêt</h1>
      <div style="width: 40%; float: left;"><img id="chevalier" src="images/chevalier.png"></img>"</div>
      <div id="dialogue">
          <p>Ce gobelin était une bonne personne</p>
          <p>car en plus de vous avoir conduit vers la sortie il vous a comté toutes sortes de boutade, créant une amitiée avec ce sacré gobelin</p>
          <form method="post" action="choix_chemin.py">
          <input type="submit" style="display: block; margin: auto;" value="SUIVANT"></form>
        </div>""")