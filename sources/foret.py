# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 14:32:20 2024

@author: rcarbonnet
"""

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_chemin.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""<h1>La forêt</h1>
      <div style="width: 40%; float: left;"><img id="chevalier" src="images/chevalier.png"></img>"</div>
      <div style="width: 40%; float: right;"><img id="gobelin" src="images/gobelin.png"></img></div>
      <div id="dialogue">
          <p>Sur la route vous appercevez un gobelin avec un air malicieu.</p>
          <p>Il vous propose, avec une voix plein de malice, un petit arrangement qui vous amènerez direct à la sortie de cette forêt qui d'après lui est très dangereuse pour seulement 10 écu</p>
          <h2>Que faites-vous ?</h2>
          <form method="post" action="foret2.py">
          <input type="submit" style="display: block; margin: auto;" value="J'ACCEPTE"></form>
          <form method="post" action="perdu2.py">
          <input type="submit" style="display: block; margin: auto;" value="JE REFUSE">
          </form>
        </div>""")