# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 15:27:35 2024

@author: rcarbonnet
"""

import cgi
import cgitb
import csv
import random
cgitb.enable()
form=cgi.FieldStorage()
html="""
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link href="styles/style_combats.css" rel="stylesheet"/>
</head>
<body>
"""
print(html)

attaque=form.getvalue("attaque")
PvJoueur=int(form.getvalue("pvjoueur"))
PvBoss=int(form.getvalue("pvboss"))
affpvjoueur=int(PvJoueur*150/100)
affpvboss=int(PvBoss*150/200)

print(f"""<div id="affichepvboss">
              <h1> PV BOSS : {PvBoss} pv  </h1>
              <div class="pv"><div style="height:10px; width: {affpvboss}px; background-color: #7FFF00;"></div>
          </div></div>
          <div id="affichepvjoueur">
              <h1> PV JOUEUR : {PvJoueur} pv </h1>
              <div class="pv"><div style="height:10px; width: {affpvjoueur}px; background-color: #7FFF00;"></div>
          </div></div>""")

print("""<div style="width: 40%; float: left;"><img id="chevalier" src="images/chevalier.png"></img>"</div><div style="width: 40%; float: right;"><img id="monstre" src="images/monstre.png"></img></div>""")

def attaqueboss():
    attaque=random.randint(1,3)
    if attaque==1:
        deg=25
    if attaque==2:
        deg=40
    if attaque==3:
        deg=-1
    return deg

def attaque1():
    chance=random.randint(0,1)
    if chance==1:
        deg=45
    if chance==0:
        deg=0
    return deg

def attaque2():
    chance=random.randint(1,5)
    if chance==1:
        deg=0
    else:
        deg=35
    return deg

if attaque=="attaque1":
    deg=attaque1()
if attaque=="attaque2":
    deg=attaque2()
print("""<div style="border: solid black 3px;
background-color: #C9C9C9;
position: fixed;
bottom: 0;
height: 30%;
width: 95%;">""")    
if deg==0:
    print("<p>Dommage ! Vous avez loupé votre coup,</p>")

if deg>0:
    print(f"""<p>Bien joué ! Vous avez infligé {deg} pv au monstre !</p>""")
    PvBoss=PvBoss-deg

degboss=attaqueboss()
    
if degboss<0:
    print("<p>Le monstre, pendant ce temps là s'est soigné de 20 pv</p>")
    PvBoss=PvBoss+20
if degboss>0:
    print(f"""<p>Pendant ce temps là, le monstre a attaqué, vous perdez {degboss} pv</p>""")
    PvJoueur=PvJoueur-degboss

if PvJoueur<0:
    print("""<form method="post" action="perdu.py">""")
elif PvBoss<0:
    print("""<form method="post" action="gagne.py" >""")
else:
    print("""<form method="post" action="pages_combats1.py">""")

print(f""" <input id="bouton_valider" type="submit" value="SUIVANT"></div>
      <input type="radio" name="pvjoueur" value="{PvJoueur}" class="cache" checked>
      <input type="radio" name="pvboss" value="{PvBoss}" class="cache" checked>""")
print("""</form></body></html>""")