# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 15:02:43 2024

@author: Rectorat Limoges
"""
import cgi
import cgitb
import csv
import pandas as pd

def reader(var,fichier):
    data = open(f"{fichier}","r")
    lecteur_csv = csv.reader(data)
    for row in lecteur_csv:
        var.append(row)
    data.close()
    return var

def writer(liste, fichier):
    data = open(f'{fichier}','w', encoding='utf_8', newline='')
    obj = csv.writer(data)
    for i in range(len(liste)):
        obj.writerow(liste[i])
        
def recuperation_de_données(fichier, n, sep=","):
    f=open(fichier, "r")
    r=csv.reader(f)
    lignes=list(r)
    f.close()
    if (n < len(lignes)) and (n >= -len(lignes)):
        return lignes[n]
    else:
        return "La variable n n'est pas valable."

def recuperation_du_nombre_de_lignes(fichier, sep=","):
    f=open(fichier, "r")
    r=csv.reader(f,delimiter=sep)
    lignes=list(r)
    return len(lignes)
"""
def modification_de_données(fichier, ligne, colonne, valeur, sep=","):
    f=open(fichier, "r+", newline="")
    r=csv.reader(f, delimiter = sep)
    donnees_du_fichier = list(r)

    donnees_du_fichier[ligne][colonne] = valeur
    f.seek(0)
    w = csv.writer(f, delimiter=sep)
    w.writerows(donnees_du_fichier)
    f.truncate()
"""

# Fonction pour modifier le fichier CSV
def modification_de_données(nom_fichier, ligne_modification, nouvelle_valeur):
    # Lire le fichier CSV
    data = pd.read_csv(nom_fichier)

    # Modifier la valeur dans la ligne spécifiée
    if ligne_modification < len(data):
        data.loc[ligne_modification, 'Armure'] = nouvelle_valeur

    # Écrire les modifications dans le fichier CSV
    data.to_csv(nom_fichier, index=False)

def trouver_la_ligne_de_l_identifiant(fichier,colonne,username):
    # Lire le fichier CSV
    data = pd.read_csv(fichier)

    # Rechercher le pseudonyme dans la colonne appropriée
    result = data[data[colonne] == username]

    # Vérifier si le pseudonyme a été trouvé
    if not result.empty:
        # Obtenir l'index de la ligne
        ligne_pseudonyme = result.index[0]
        return ligne_pseudonyme
    else:
        print("Le pseudonyme n'a pas été trouvé dans le fichier.")

def trouver_si_l_identifiant_est_deja_pris(fichier, username):
    with open(fichier, 'r') as f:
        lecture = csv.reader(f)
        for row in lecture:
            if row[0] == username:
                return True
    return False

