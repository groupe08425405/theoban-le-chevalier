# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 00:22:04 2024

@author: robca
"""

html="""
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link href="styles/style_combats.css" rel="stylesheet"/>
</head>
<body>
    <h1 style="font-size: 200px; color: red; text-align:center;">GAME OVER</h1>
    <h3 style="color: white; text-align: center;">Vous avez succombé au monstre, </br> retenter votre chance !</h3>
    <form method="post" action="index.py">
    <input id="bouton_valider" type="submit" value="ACCEUIL">
    </form>
</body>
</html>
"""
print(html)
