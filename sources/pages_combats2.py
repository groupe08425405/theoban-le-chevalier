# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 15:23:35 2024

@author: rcarbonnet
"""

import cgi
import cgitb
import csv
cgitb.enable()
form=cgi.FieldStorage()
html="""
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> Ma page </title>
    <link href="styles/style_combats.css" rel="stylesheet"/>
</head>
<body>
"""
print(html)

choix=form.getvalue("choix")
pvjoueur=int(form.getvalue("pvjoueur"))
pvboss=int(form.getvalue("pvboss"))

affpvjoueur=int(pvjoueur*150/100)
affpvboss=int(pvboss*150/200)

print(f"""<div id="affichepvboss">
              <h1> PV BOSS : {pvboss} pv  </h1>
              <div class="pv"><div style="height:10px; width: {affpvboss}px; background-color: #7FFF00;"></div>
          </div></div>
          <div id="affichepvjoueur">
              <h1> PV JOUEUR : {pvjoueur} pv </h1>
              <div class="pv"><div style="height:10px; width: {affpvjoueur}px; background-color: #7FFF00;"></div>
          </div></div>""")
print("""<div style="width: 40%; float: left;"><img id="chevalier" src="images/chevalier.png"></img>"</div><div style="width: 40%; float: right;"><img id="monstre" src="images/monstre.png"></img></div>""")

if choix=="fuire":
    print("""
                <form method="post" action="pages_combats1.py">
                <div id="interface">
                    <h1>La fuite n'est pas une option soldat! </br> retourner au combat</h1>
                    <input id="bouton_valider" type="submit" value="REVENIR">
                </div>""")
else:
    print("""
          <div style="border: solid black 3px;
          background-color: #C9C9C9;
          position: fixed;
          bottom: 0;
          height: 30%;
          width: 95%;">>
          <form method="post" action="pages_combats3.py">
          <h2>Choisissez votre attaque !</h2>
          <div id="boutongauche"><input type="radio" name="attaque" id="boutton1" value="attaque1" checked><label for="boutton1" checked> ATTAQUE LOURDE</label></div>
          <div id="boutondroite"><input type="radio" name="attaque" id="boutton2" value="attaque2"><label for="boutton2"> ATTAQUE LEGERE</label></div>
          <input type="submit" id="bouton_valider" value="ATTAQUER">
          </div>
          """)
      

print(f"""<input type="radio" name="pvjoueur" value="{pvjoueur}" class="cache" checked>
      <input type="radio" name="pvboss" value="{pvboss}" class="cache" checked>""")
print("""</form></body></html>""")