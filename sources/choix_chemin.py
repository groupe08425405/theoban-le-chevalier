# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 14:37:37 2024

@author: rcarbonnet
"""

import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb


# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
	<meta charset="UTF_8">
	<title> Ma page Web </title>
    <link href="styles/style_page2.css" rel="stylesheet" />
    </head>
    <body>
    """)

print("""
<h1>Chapitre 2 : </h1>
<p>Après toutes ces aventures, tu veux trouver un endroit pour te reposer, mais deux chemins se présentent devant toi.</p>

<form action="auberge.py">
    <button type="submit">Tu prend le chemin dans la fôret sombre.</button>
</form><form method="post" action="perdu_nuit.py">
    <button type="submit">Tu prend le chemin très éclairés et passant dans des prairies.</button>
</form>""")

print("""</body>
</html>
""")

