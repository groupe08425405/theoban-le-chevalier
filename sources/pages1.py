# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 16:06:45 2024

@author: rcarbonnet
"""

import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

username=form.getvalue("username")


html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)
print("""<head>
 	  <meta charset="UTF_8">
 	  <title> Ma page Web </title>
      <link href="styles/style_page1_1_.css" rel="stylesheet" />
      </head>
      <body>
      """)

#Texte du chapitre + choix
print("""<h1>Chapitre 1 : L'adoubement</h1>
  <p>Bonjour Théoban !</p>
  <p>Tu viens de te te faire adouber par le roi.</p>
  <p>Il te propose deux armures au choix avec laquelle commencer ton périple, mais pour l'instant tu n'as que 1 000 <img id="ecu" src="images/ecu.png">.</p>
  <form action="chapitre2.py">
  <button value="or" name="armure" type="submit">Une armure en or pour montrer sa richesse à 500 <img id="ecu" src="images/ecu.png">.</br></br><img id="armures" src="images/armure_doree.avif"></button>
  <button value="argent" name="armure" type="submit">Une armure en argent très solide pour être le plus protégé possible à 300 <img id="ecu" src="images/ecu.png">.</br></br><img id="armures" src="images/armure_argent.png"></button>
  </form>""")

html="""</body>
</html>
"""

print(html)
